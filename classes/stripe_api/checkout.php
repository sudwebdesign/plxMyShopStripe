<?php if (!defined('PLX_ROOT')) exit;# $this is plxMyShop instance
$inTest = false;
$apiName = basename(__DIR__);
$aData['devise'] = $devise = $this->getParam('payment_stripe_currencycode_' . $this->dLang);
$aData['montant'] = $montant = $totalpricettc;
$pkStripe = $this->getParam('payment_stripe_user');# (public ApiKey)
$aData['pkStripe'] = $this->getParam('payment_stripe_key');# (private ApiKey)
$aData['livraison'] = $livraison = $totalpoidgshipping;
$aData['nomClient'] = $nomClient = "{$_POST['firstname']} {$_POST['lastname']}";
$aData['prodsList'] = strip_tags(str_replace('<br/>', ' ',$messCommon));
$aData['myshopmail'] = $TONMAIL;
$aData['stripe_types'] = $this->getParam('payment_stripe_types');
$aData['productstock'] = array();#for cancel order
# Products list to stripe + reset stock if cancel
$purchase = array();
foreach($productscart as $pId => $p) {
 if($this->aProds[$this->dLang][$pId]['iteminstock'] != ''){
  $aData['productstock'][$pId] = 0 - $p['nombre'];# for reset stock
 }
 $purchase[$pId] = array(
   $p['nombre']
  ,$this->pos_devise($p['pricettc'])
  ,$this->smartWeight($p['poidg'])
  ,$p['name']
 );
}#Fi foreach
$purchase[$this->getLang('L_TOTAL_BASKET')] = array(
  count($productscart)
 ,$this->pos_devise($totalpricettc)
 ,$this->smartWeight($totalpoidg)
 ,$this->pos_devise($totalpoidgshipping)
);
$maxtry = empty($plxPlugin->getParam('payment_stripe_maxtry'))? 10: intval($plxPlugin->getParam('payment_stripe_maxtry'));
$abortpayment = intval($plxPlugin->getParam('payment_stripe_abortpayment'));
$cancelorder = intval($plxPlugin->getParam('payment_stripe_cancelorder'));

$_SESSION[$apiName] = $aData;#store :need in create.php (maybe rename in pay or ajax)
# Maybe never used (is in ajax)
# $urlRetour = $this->getParam('payment_stripe_returnurl');
# $urlAnnulation = $this->getParam('payment_stripe_cancelurl');

ob_start();
#src : https://stripe.com/docs/payments/integration-builder + script : https://jsfiddle.net/4zc1uuas/ :from: https://stackoverflow.com/questions/46637483/dynamically-load-stripe-js
?>
 <p id="card-errors" role="alert"></p>
 <button id="showBtn"><?php $plxPlugin->lang('L_SHOW_PAYMENT_FORM') ?></button>
 <form id="payment-form" style="display:none;">
  <p class="abortpayment-message hidden">
   <?php $plxPlugin->lang('L_STRIPE_ABORTED') ?>
   <a href=""><?php $plxPlugin->lang('L_STRIPE_GO_HOME') ?>.</a>
  </p>
  <p class="cancelorder-message hidden">
   <?php $plxPlugin->lang('L_STRIPE_CANCELED') ?>
   <a href=""><?php $plxPlugin->lang('L_STRIPE_GO_HOME') ?>.</a>
  </p>
  <div id="card-element"><!--Stripe.js injects the Card Element--></div>
  <button id="submitPay">
   <div class="spinner hidden" id="spinner"></div>
   <span id="button-text"><?php $plxPlugin->lang('L_PAY') ?> <span id="button-tot"><?php echo $this->pos_devise($montant) ?></span></span>
  </button>
  <p class="result-message hidden">
   <?php $plxPlugin->lang('L_STRIPE_SUCCESS') ?>
<?php if(strpos($pkStripe, 'test') !== FALSE): $inTest=true; ?>
   <a href="" target="_blank"><?php $plxPlugin->lang('L_STRIPE_DASH') ?>.</a>
<?php else: ?>
   <a href=""><?php $plxPlugin->lang('L_STRIPE_GO_HOME') ?>.</a>
<?php endif; ?>
  </p>
 </form>
 <p>
<?php if($abortpayment){ ?>
  <a id="abortpayment" href="javascript:;" style="display:none"><?php $plxPlugin->lang('L_ABORT') ?></a>
<?php } if($cancelorder){ ?>
  <a id="cancelorder" href="javascript:;" style="display:none"><?php $plxPlugin->lang('L_CANCEL') ?></a>
<?php }?>
 </p>
<!-- <p><img src="<?php echo $this->plxMotor->racine . PLX_PLUGINS . get_class($plxPlugin);?>/images/stripe_logo.gif" alt="Stripe"/></p> -->
 <p title="Stripe"><svg style="color:#5469d4" height="32" width="77" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 360 150"><path fill-rule="evenodd" d="M360 77.4c0 2.4-.2 7.6-.2 8.9h-48.9c1.1 11.8 9.7 15.2 19.4 15.2 9.9 0 17.7-2.1 24.5-5.5v20c-6.8 3.8-15.8 6.5-27.7 6.5-24.4 0-41.4-15.2-41.4-45.3 0-25.4 14.4-45.6 38.2-45.6 23.7 0 36.1 20.2 36.1 45.8zm-49.4-9.5h25.8c0-11.3-6.5-16-12.6-16-6.3 0-13.2 4.7-13.2 16zm-63.5-36.3c17.5 0 34 15.8 34.1 44.8 0 31.7-16.3 46.1-34.2 46.1-8.8 0-14.1-3.7-17.7-6.3l-.1 28.3-25 5.3V33.2h22l1.3 6.2c3.5-3.2 9.8-7.8 19.6-7.8zm-6 68.9c9.2 0 15.4-10 15.4-23.4 0-13.1-6.3-23.3-15.4-23.3-5.7 0-9.3 2-11.9 4.9l.1 37.1c2.4 2.6 5.9 4.7 11.8 4.7zm-71.3-74.8V5.3L194.9 0v20.3l-25.1 5.4zm0 7.6h25.1v87.5h-25.1V33.3zm-26.9 7.4c5.9-10.8 17.6-8.6 20.8-7.4v23c-3.1-1.1-13.1-2.5-19 5.2v59.3h-25V33.3h21.6l1.6 7.4zm-50-29.1l-.1 21.7h19v21.3h-19v35.5c0 14.8 15.8 10.2 19 8.9v20.3c-3.3 1.8-9.3 3.3-17.5 3.3-14.8 0-25.9-10.9-25.9-25.7l.1-80.1 24.4-5.2zM25.3 58.7c0 11.2 38.1 5.9 38.2 35.7 0 17.9-14.3 28.2-35.1 28.2-8.6 0-18-1.7-27.3-5.7V93.1c8.4 4.6 19 8 27.3 8 5.6 0 9.6-1.5 9.6-6.1 0-11.9-38-7.5-38-35.1 0-17.7 13.5-28.3 33.8-28.3 8.3 0 16.5 1.3 24.8 4.6v23.5c-7.6-4.1-17.2-6.4-24.8-6.4-5.3 0-8.5 1.5-8.5 5.4z"></path></svg></p>
 <p><img src="<?php echo $this->plxMotor->racine . PLX_PLUGINS . get_class($plxPlugin);?>/icon.big.png" alt=""/></p>
 <script type="text/JavaScript">
var showBtn = document.querySelector('#showBtn');
//~ showBtn.addEventListener('click', loadFiles);
showBtn.onclick = loadFiles;
numtry = 0;
maxtry = <?php $maxtry;?>;
abortpayment = <?php $abortpayment;?>;
cancelorder = <?php $cancelorder;?>;

//called in client js
inTest = <?php echo intval($inTest) ?>;
pkStripe = '<?php echo $pkStripe ?>';//public api key pk_test_rWG7KN6nzFjUj5D0EkbdevdS
plx_uri = '<?php echo $this->plxMotor->racine;?>'
php_uri = plx_uri + '<?php echo PLX_PLUGINS . get_class($plxPlugin);?>/classes/stripe_api/';
plg_ver = '?v=<?php echo $plxPlugin::V;?>';
create_uri = php_uri + 'create.php';
l_paid = '<?php $plxPlugin->lang('L_PAID') ?>';
l_aborted = '<?php $plxPlugin->lang('L_ABORTED') ?>';
l_canceled = '<?php $plxPlugin->lang('L_CANCELED') ?>';
l_load_error = '<?php $plxPlugin->lang('L_LOAD_ERROR') ?>';
l_reload = '<?php $plxPlugin->lang('L_RELOAD') ?>';
// The items the customer wants to buy
var purchase = {
  items: [<?php echo json_encode($purchase) ?>]
};
function loadFiles() {
  showBtn.style.visibility = 'hidden';
  showBtn.onclick = function(){};
  loadScript(php_uri + 'global.css' + plg_ver, '', 'link');
  //loadScript('https://js.stripe.com/v3/');
  loadScript(php_uri + 'client.js' + plg_ver, 'defer');
  //~ document.querySelector('#payment-form').style.display = '';
  if(abortpayment) document.querySelector('#abortpayment').style.display = '';
  if(cancelorder) document.querySelector('#cancelorder').style.display = '';
}
function loadScript(url, attrs, type, charset) {
  if (type === undefined) type = 'script';
  if (url) {
    var script = document.querySelector("script[src*='" + url + "']");
    if (!script) {
      var heads = document.getElementsByTagName('head');
      if (heads && heads.length) {
        var head = heads[0];
        if (head) {
          script = document.createElement(type);
          if (type == 'link') {
            script.setAttribute('rel', 'stylesheet');
            script.setAttribute('href', url);
          }else{
            script.setAttribute('src', url);
          }
          if (charset) script.setAttribute('charset', charset);
          if (attrs) script.setAttribute(attrs, attrs);
          head.appendChild(script);
        }
      }
    }
    return script;
  }
};
//from https://jsfiddle.net/4zc1uuas/
loadStripe = function() {//UNUSED
  var stripe = Stripe('<?php echo $pkStripe ?>');//pk_test_rWG7KN6nzFjUj5D0EkbdevdS
  var elements = stripe.elements();

  var card = elements.create('card', {
    style: {
      base: {
        iconColor: '#666EE8',
        color: '#31325F',
        lineHeight: '40px',
        fontWeight: 400,
        fontFamily: '"Proxima Nova", "Montserrat", Arial, sans-serif',
        fontSize: '16px',

        '::placeholder': {
          color: '#CFD7E0',
        },
      },
    }
  });
  card.mount('#card-element');

  function setOutcome(result) {
    var successElement = document.querySelector('.success');
    var errorElement = document.querySelector('.error');
    successElement.classList.remove('visible');
    errorElement.classList.remove('visible');

    if (result.token) {
      // Use the token to create a charge or a customer
      // https://stripe.com/docs/charges

      successElement.querySelector('.token').textContent = result.token.id;
      successElement.classList.add('visible');
      //self.confetti();
    } else if (result.error) {
      errorElement.textContent = result.error.message;
      errorElement.classList.add('visible');
    }
  }

  card.on('change', function(event) {
    setOutcome(event);
  });

  document.querySelector('form').addEventListener('submit', function(e) {
    e.preventDefault();
    var form = document.querySelector('form');
    var extraDetails = {
      name: form.querySelector('input[name=cardholder-name]').value,
    };
    stripe.createToken(card, extraDetails).then(setOutcome);
  });
}

 </script>
 <script src="https://js.stripe.com/v3/" onload="loadFiles();"></script>
<?php
$msgCommand .= ob_get_clean();