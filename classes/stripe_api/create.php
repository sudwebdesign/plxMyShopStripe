<?php
$apiName = basename(__DIR__);
session_start();
if(!isset($_SESSION['stripe'])) exit;
$command_filename = 'command_filename' . $_SESSION['stripe'][0] . $_SESSION['stripe'][1];
if(!empty($_GET['paid']) && isset($_SESSION[$command_filename])){
 $nf = $_SESSION[$command_filename];
 $paid = $_GET['paid'];# Get lang : L_PAID | L_ABORTED | L_CANCELED
 $abort = !empty($_GET['abortpayment']);
 $cancel = !empty($_GET['cancelorder']);
 $color = 'dark' . ($abort? 'orange':($cancel?'red':'green'));
 $style = 'text-decoration: ' . ($abort? 'overline': 'line-through');
 $command = file_get_contents($nf);
 $command = str_replace('<body>', '<body>'.PHP_EOL.'<b style="color:'.$color.'"><i>'.$paid.' (' .date('Y.m.d H:i:s'). ')</i></b>'.PHP_EOL, $command);
 $command = str_replace('<span class="cust_mess">', '<span class="cust_mess" style="'.$style.'">', $command);
 if(!empty($command)) file_put_contents($nf, $command);
 if(is_writable($nf)) {
  if(!$abort) {
   $nfn = str_replace('A.html', ($cancel?'C':'P').'.html',$nf);# Await, Cancel, Paid
   $result = rename($nf, $nfn);
  }
  if($cancel AND !empty($_SESSION[$apiName]['productstock'])) {# Cancel order idea (need increase stock)
   $_SESSION['cancel_stripe']['productstock'] = $_SESSION[$apiName]['productstock'];
  }
 }
 unset($_SESSION['command_filename'], $_SESSION[$apiName], $_SESSION[$_SESSION['stripe'][0]], $_SESSION['stripe']);
 exit;
}
if(!isset($_SESSION[$apiName]['pkStripe'])) exit;

require 'vendor/autoload.php';

// This is a sample test API key. Sign in to see examples pre-filled with your key.
\Stripe\Stripe::setApiKey($_SESSION[$apiName]['pkStripe']);//private api (secret) key 'sk_test_4eC39HqLyjWDarjtT1zdp7dc'

header('Content-Type: application/json');

try {
  // retrieve JSON from POST body
  //~ $json_str = file_get_contents('php://input');
  //~ $json_obj = json_decode($json_str);

  $paymentIntent = \Stripe\PaymentIntent::create([
    //~ 'amount' => calculateOrderAmount($json_obj->items),
    'amount' => $_SESSION[$apiName]['montant']*100,#to cents
    'currency' => strtolower($_SESSION[$apiName]['devise']),//usd
    'description' => $_SESSION[$apiName]['prodsList'],
    'payment_method_types' => explode(',', $_SESSION[$apiName]['stripe_types']),
    'receipt_email' => $_SESSION[$apiName]['myshopmail']
  ]);

  $output = [
    'clientSecret' => $paymentIntent->client_secret,
  ];

  echo json_encode($output);
} catch (Error $e) {
  http_response_code(500);
  echo json_encode(['error' => $e->getMessage()]);
}