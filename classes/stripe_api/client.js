// Disable the #submitPay until we have Stripe set up on the page
document.querySelector('#submitPay').disabled = true;
document.querySelector('#payment-form').style.display = '';
if(!!window.Stripe) {//is loaded
  showBtn.style.display = 'none';
  var stripe = Stripe(pkStripe);//public apikey 'pk_'
  fetch(create_uri, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(purchase)//tep
  })
    .then(function(result) {
      return result.json();
    })
    .then(function(data) {
      var elements = stripe.elements();

      var style = {
        base: {
          color: '#32325d',
          fontFamily: 'Arial, sans-serif',
          fontSmoothing: 'antialiased',
          fontSize: '16px',
          '::placeholder': {
            color: '#32325d'
          }
        },
        invalid: {
          fontFamily: 'Arial, sans-serif',
          color: '#fa755a',
          iconColor: '#fa755a'
        }
      };

      var card = elements.create('card', { style: style });
      // Stripe injects an iframe into the DOM
      card.mount('#card-element');

      card.on('change', function (event) {
        // Disable the Pay button if there are no card details in the Element
        document.querySelector('#submitPay').disabled = event.empty;
        document.querySelector('#card-errors').textContent = event.error ? event.error.message : '';
      });

      var form = document.querySelector('#payment-form');
      form.addEventListener('submit', function(event) {
        event.preventDefault();
        // Complete payment when the submit #submitPay is clicked
        payWithCard(stripe, card, data.clientSecret);
      });
    });
}// Fi Stripe loaded
else{//Stripe not loaded
  document.querySelector('#submitPay').classList.add('hidden');
  document.querySelector('#card-element').classList.add('hidden');
  showBtn.onclick = function(){window.location = window.location;};
  showBtn.textContent = l_reload;
  setTimeout(function() {
    showError(l_load_error);
    showBtn.style.display = 'none';
  }, 222);
  setTimeout(function() {
    showError(l_load_error);
    showBtn.style.visibility = '';
    showBtn.style.display = '';
  }, 4222);
}
// Calls stripe.confirmCardPayment
// If the card requires authentication Stripe shows a pop-up modal to
// prompt the user to enter authentication details without leaving your page.
var payWithCard = function(stripe, card, clientSecret) {
  loading(true);
  stripe
    .confirmCardPayment(clientSecret, {
      payment_method: {
        card: card
      }
    })
    .then(function(result) {
      if (result.error) {
        // Show error to your customer
        showError(result.error.message);
      } else {
        // The payment succeeded!
        orderComplete(result.paymentIntent.id);
      }
    });
};

/* ------- UI helpers ------- */

// Shows a success message when the payment is complete
var orderComplete = function(paymentIntentId) {
  loading(false);
  document.querySelector('.result-message').classList.remove('hidden');
  document.querySelector('#submitPay').disabled = true;
  fetch(create_uri + '?paid=' + l_paid, {
    method: 'GET',
  })
  .then(function(data) {
    l_paid = l_paid + ' ' + document.querySelector('#button-tot').innerHTML;
    document.querySelector('#button-text').innerHTML = l_paid;
    document.querySelector('.msgyeah').innerHTML = l_paid;
    document.querySelector('#abortpayment').classList.add('hidden');
    document.querySelector('#cancelorder').classList.add('hidden');
    if(inTest)
     document
      .querySelector('.result-message a')
      .setAttribute(
        'href',
        'https://dashboard.stripe.com/test/payments/' + paymentIntentId
      );
  });
};

// Shows a message when the client click on abort or cancel (or maxtry)
var orderAbort = function(lang,type) {
  loading(false);
  document.querySelector('.'+type+'-message').classList.remove('hidden');
  document.querySelector('#card-element').classList.add('hidden');
  document.querySelector('#submitPay').disabled = true;
  document.querySelector('#submitPay').classList.add('hidden');
  document.querySelector('#showBtn').classList.add('hidden');
  fetch(create_uri + '?paid=' + lang + '&'+type+'=yes', {
    method: 'GET',
  })
  .then(function(data) {
    l_msg = lang;// + ' ' + document.querySelector('#button-tot').innerHTML;
    if(abortpayment) document.querySelector('#abortpayment').classList.add('hidden');
    if(cancelorder) document.querySelector('#cancelorder').classList.add('hidden');
    document.querySelector('#button-text').innerHTML = l_msg;
    document.querySelector('.msgyeah').innerHTML = l_msg;
    if(type.search('order')>0)//order canceled
     fetch(plx_uri, {method:'GET'});//if need, restore stocks
  });
};

// Show the customer the error from Stripe if their card fails to charge
var showError = function(errorMsgText) {
  loading(false);
  if(numtry <= maxtry){
    numtry = ++numtry;
    var errorMsg = document.querySelector('#card-errors');
    errorMsg.textContent = errorMsgText;
    setTimeout(function() {
      errorMsg.textContent = '';
    }, 4000);
  }else{
    orderAbort(l_aborted, 'abortpayment');
  }
};

// Show a spinner on payment submission
var loading = function(isLoading) {
  if (isLoading) {
    // Disable the #submitPay and show a spinner
    document.querySelector('#submitPay').disabled = true;
    document.querySelector('#spinner').classList.remove('hidden');
    document.querySelector('#button-text').classList.add('hidden');
  } else {
    document.querySelector('#submitPay').disabled = false;
    document.querySelector('#spinner').classList.add('hidden');
    document.querySelector('#button-text').classList.remove('hidden');
  }
};
if(abortpayment) document.querySelector('#abortpayment').onclick = function(e){return confirm(e.target.textContent+'?')?orderAbort(l_aborted, 'abortpayment'):false;};
if(cancelorder) document.querySelector('#cancelorder ').onclick = function(e){return confirm(e.target.textContent+'?')?orderAbort(l_canceled, 'cancelorder'):false;};