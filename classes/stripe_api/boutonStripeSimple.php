<?php if (!defined('PLX_ROOT')) exit;
$montant = $totalpricettc;
$livraison = $totalpoidgshipping;
$devise = $plxPlugin->getParam("payment_stripe_currencycode");
$urlRetour = $plxPlugin->getParam("payment_stripe_returnurl");
$urlAnnulation = $plxPlugin->getParam("payment_stripe_cancelurl");
$adresseEmailStripe = $plxPlugin->getParam("payment_stripe_user");;
$nomClient = "{$_POST["firstname"]} {$_POST["lastname"]}";
ob_start();
?>
 <form id="stripe_form" action="https://www.stripe.com/cgi-bin/webscr" method="post">
  <input type="hidden" name="amount" value="<?php echo htmlspecialchars($montant);?>"/>
  <input type="hidden" name="currency_code" value="<?php echo htmlspecialchars($devise);?>"/>
  <input type="hidden" name="shipping" value="<?php echo htmlspecialchars($livraison);?>"/>
  <input type="hidden" name="tax" value="0.00"/>
  <input type="hidden" name="return" value="<?php echo htmlspecialchars($urlRetour);?>"/>
  <input type="hidden" name="cancel_return" value="<?php echo htmlspecialchars($urlAnnulation);?>"/>
  <input type="hidden" name="cmd" value="_xclick"/>
  <input type="hidden" name="business" value="<?php echo htmlspecialchars($adresseEmailStripe);?>"/>
  <input type="hidden" name="item_name" value="<?php $plxPlugin->lang('L_COMMAND_OF'); ?> <?php echo htmlspecialchars($nomClient);?>"/>
  <input type="hidden" name="no_note" value="0"/>
  <input type="hidden" name="lc" value="<?php echo strtoupper($plxPlugin->default_lang); ?>"/>
  <input type="hidden" name="bn" value="PP-BuyNowBF"/>
  <input
   type="image"
   name="submit"
   onClick="postFormPayPal();"
   style="width:auto;height:auto;border:none;"
   alt="<?php $plxPays->lang('STRIPE_ALT'); ?>"
   src="https://www.stripeobjects.com/<?php $plxPays->lang('STRIPE_IMG'); ?>/i/btn/btn_buynow_LG.gif"
  />
 </form>
 <p><img src="<?php echo $plxPlugin->plxMotor->racine . PLX_PLUGINS . get_class($plxPays);?>/icon.big.png" alt=""/></p>
 <p><img src="<?php echo $plxPlugin->plxMotor->racine . PLX_PLUGINS . get_class($plxPays);?>/images/stripe_logo.gif" alt=""/></p>
 <script type="text/JavaScript">
  function postFormPayPal() {
   var stripe_form = document.getElementById("stripe_form");
   stripe_form.style.display ="none";
   stripe_form.submit();
  }
 </script>
<?php
$msgCommand .= ob_get_clean();