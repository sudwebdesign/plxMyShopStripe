<?php //sample
/*
Next let’s create the form that’s going to do the charging. We’re going to use Checkout, an embedded HTML form that will take care of form validation, error handling, and sending credit card numbers securely to Stripe.
*/
require_once('./config.php');
?>

<form action="charge.php" method="post">
  <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
          data-key="<?php echo $stripe['publishable_key']; ?>"
          data-description="Access for a year"
          data-amount="5000"
          data-locale="auto"></script>
</form>
<?php
/*
Notice we’re passing through an amount and description. These are just for display purposes only, they don’t get passed onto Stripe. Instead, once the user completes the Checkout process, a hidden field will be inserted into the form called stripeToken, and then the form submitted to the server.

In charge.php, we can use the stripeToken POST parameter to actually charge the card:
*/




  //require_once('./config.php');

  $token  = $_POST['stripeToken'];
  $email  = $_POST['stripeEmail'];

  $customer = \Stripe\Customer::create(array(
      'email' => $email,
      'source'  => $token
  ));

  $charge = \Stripe\Charge::create(array(
      'customer' => $customer->id,
      'amount'   => 5000,#here
      'currency' => 'usd'#here
  ));

  echo '<h1>Successfully charged $50.00!</h1>';#here $50.00 € F
