<?php if (!defined('PLX_ROOT')) exit;
/**
 * Class plxMyShopStripe, add stripe payment in your plxMyShop
 * @version 1.0.1 * @date 24/07/2020 @author        Thomas Ingles
 **/
class plxMyShopStripe extends plxPlugin {
  const V = '1.0.1';
  public function __construct($default_lang){
    parent::__construct($default_lang);# appel du constructeur de la classe plxPlugin (obligatoire)
    if(defined('PLX_ADMIN')) {#Déclaration des hooks pour la zone d'administration
      $this->addHook('plxMyShopConfigPost', 'plxMyShopConfigPost');
      $this->addHook('plxMyShopConfigInit', 'plxMyShopConfigInit');
      $this->addHook('plxMyShopConfig', 'plxMyShopConfig');
      $this->addHook('plxMyShopConfigPayments', 'plxMyShopConfigPayments');
    }else{#SESSIONS
      $this->addHook('plxMyShopMotorConstruct', 'plxMyShopMotorConstruct');#Restore stock if command is canceled
      $this->addHook('plxMyShopValiderCommandeEnd', 'plxMyShopValiderCommandeEnd');#GET $_SESSION[$plxPlugin->plugName]['msgCommand']
      $this->addHook('plxMyShopPanierDebut', 'plxMyShopPanierDebut');#RESTORE $_SESSION[$plxPlugin->plugName]['msgCommand']
      $this->addHook('ThemeEndHead', 'ThemeEndHead');#hide command form & product list
      $this->addHook('plxMyShopValiderCommande', 'plxMyShopValiderCommande');
      $this->addHook('plxMyShopValiderCommandePost', 'plxMyShopValiderCommandePost');#add $method pay lang
      $this->addHook('plxMyShopValiderCommandeEmailSwitchPayments', 'plxMyShopValiderCommandeEmailSwitchPayments');
      $this->addHook('plxMyShopValiderCommandeEmailPayments', 'plxMyShopValiderCommandeEmailPayments');
      $this->addHook('plxMyShopValiderCommandeEmailPaymentsClient', 'plxMyShopValiderCommandeEmailPaymentsClient');
      $this->addHook('plxMyShopPanierSelectMethodPayment', 'plxMyShopPanierSelectMethodPayment');
    }
  }
  # Appelé par client.js par ajax
  public function plxMyShopMotorConstruct(){# Augmente le stock des produits d'une commande annulée
   if(!empty($_SESSION['cancel_stripe']['productstock'])){# Si necessaire
echo '<?php '; ?>
    # stripe
    $pms = $this->plxPlugins->aPlugins['plxMyShop'];
    $aLangs = ($pms->aLangs)?$pms->aLangs:array($pms->dLang);
    # Si noaddcart c'est enclenché
    foreach ($_SESSION['cancel_stripe']['productstock'] as $pId => $nb){
     if(empty($pms->aProds[$pms->dLang][$pId]['iteminstock'])){
      foreach($aLangs as $lang) {#4 all langs
       $pms->aProds[$lang][$pId]['iteminstock'] = 1;#increase stock to 1
       $pms->aProds[$lang][$pId]['noaddcart'] = '';#auto show basket button
       $_SESSION['cancel_stripe']['productstock'][$pId] = $nb + 1;#remove 1, is negagive ;)
      }
     }
    }
    # Recharge les stocks
    $pms->editStocks($_SESSION['cancel_stripe']['productstock']);
    unset($pms, $pId, $nb, $_SESSION['cancel_stripe']);
    # end stripe
?><?php
   }
  }
 public function onUpdate(){#mise a jour de aParam de plxMyShop pour passé a la v1.0.1 (plxMyShop est placé avant moi, ça le fait, sinon il n'est pas instancié et ne met pas à jour!)
  $plxMotor = @plxMotor::getInstance();
  $plxMyShop = empty($plxMotor->plxPlugins->aPlugins['plxMyShop'])?FALSE:$plxMotor->plxPlugins->aPlugins['plxMyShop'];#get instance | false
  if($plxMyShop){
   $param = 'payment_stripe_currencycode';#1.0.0
   if(isset($plxMyShop->aParams[$param])) {
    $currencycode = $plxMyShop->getParam($param);
    if($currencycode){#up to 1.0.1
     $plxMyShop->setParam('payment_stripe_currencycode_' . $plxMyShop->dLang, $currencycode, 'string');#up to 1.0.1
    }
    $plxMyShop->delParam($param);#unset($plxMyShop->aParams[$param]); #aParams is protected
   }#1.0.0
   if(!defined('L_SAVE_SUCCESSFUL')) @define('L_SAVE_SUCCESSFUL', __CLASS__ .' &gt;&gt;&gt; '.self::V.' ✔');
   include_once(PLX_CORE.'/lib/class.plx.msg.php');
   $plxMyShop->saveParams();# @ hide little error for public mode
  }#plxMyShop
  #return array('cssCache' => true);# not need here ;)
 }
  public function plxMyShopValiderCommandeEnd(){#GET $_SESSION[$plxPlugin->plugName]['msgCommand']
echo '<?php '; ?>
    # stripe
    if(empty($_SESSION[$this->plugName]['errorPost']) AND $_POST['methodpayment'] == 'stripe'){#$_SESSION[__CLASS__]['methodpayment']
     $_SESSION['stripe'] = array($this->plugName, $this->dLang);# 4 payment (used in create.php)
     $_SESSION[$this->plugName]['msgCommand<?=__CLASS__?>'] = $msgCommand;#$_SESSION[$this->plugName]['msgCommand']
    }
    # end stripe
?><?php
 }
  public function ThemeEndHead(){
   if(isset($_SESSION['stripe'])){
    ?><style>#myShopCart{display:none;}</style><?php
   }
  }
  public function plxMyShopPanierDebut(){#RESTORE $_SESSION[$plxPlugin->plugName]['msgCommand']
echo '<?php '; ?>
    # stripe
    if ( isset($_SESSION[$plxPlugin->plugName]['msgCommand<?=__CLASS__?>'])
     && !empty($_SESSION[$plxPlugin->plugName]['msgCommand<?=__CLASS__?>'])
    ){
      $afficheMessage = TRUE;
      $message = $_SESSION[$plxPlugin->plugName]['msgCommand<?=__CLASS__?>'];
      //~ unset($_SESSION[$plxPlugin->plugName]['msgCommand<?=__CLASS__?>']);#IF PAY OK (BY JS create.php) (TEP)
      $_SESSION[$plxPlugin->plugName]['msgCommand'] = $message;
      $_SESSION[$plxPlugin->plugName]['prods'][$plxPlugin->dLang] = $_SESSION[$plxPlugin->plugName]['prods<?=__CLASS__?>'][$plxPlugin->dLang];
      $_SESSION[$plxPlugin->plugName]['ncart'][$plxPlugin->dLang] = $_SESSION[$plxPlugin->plugName]['ncart<?=__CLASS__?>'][$plxPlugin->dLang];
    }
    # end stripe
?><?php
 }
  public function plxMyShopConfigPost(){#plxMyShop Config
echo '<?php '; ?>
    # stripe
    $plxPlugin->setParam('payment_stripe', isset($_POST['payment_stripe'])?'1':'0', 'numeric');
    $plxPlugin->setParam('payment_stripe_currencycode'.$lng, $_POST['payment_stripe_currencycode'], 'string');
    # prod
    $plxPlugin->setParam('payment_stripe_amount', $_POST['payment_stripe_amount'], 'string');
    $plxPlugin->setParam('payment_stripe_user', $_POST['payment_stripe_user'], 'string');
    $plxPlugin->setParam('payment_stripe_key', $_POST['payment_stripe_key'], 'string');
    $plxPlugin->setParam('payment_stripe_types', $_POST['payment_stripe_types'], 'string');
    # aborts
    $plxPlugin->setParam('payment_stripe_maxtry', $_POST['payment_stripe_maxtry'], 'numeric');
    $plxPlugin->setParam('payment_stripe_abortpayment', isset($_POST['payment_stripe_abortpayment'])?'1':'0', 'numeric');
    $plxPlugin->setParam('payment_stripe_cancelorder', isset($_POST['payment_stripe_cancelorder'])?'1':'0', 'numeric');
    # end stripe
?><?php
  }
  public function plxMyShopConfigInit(){
echo '<?php '; ?>
    # stripe
    $var['payment_stripe'] = intval($plxPlugin->getParam('payment_stripe'));
    $var['payment_stripe_currencycode'] = empty($plxPlugin->getParam('payment_stripe_currencycode'.$lng)) ? '<?php $this->lang('CURRENCYCODE');?>' : $plxPlugin->getParam('payment_stripe_currencycode'.$lng);
    # prod
    $var['payment_stripe_amount'] = empty($plxPlugin->getParam('payment_stripe_amount')) ? '0.5' : $plxPlugin->getParam('payment_stripe_amount');
    $var['payment_stripe_user'] = empty($plxPlugin->getParam('payment_stripe_user')) ? 'pk_test_rWG7KN6nzFjUj5D0EkbdevdS' : $plxPlugin->getParam('payment_stripe_user');
    $var['payment_stripe_key'] = empty($plxPlugin->getParam('payment_stripe_key')) ? 'sk_test_4eC39HqLyjWDarjtT1zdp7dc' : $plxPlugin->getParam('payment_stripe_key');
    $var['payment_stripe_types'] = empty($plxPlugin->getParam('payment_stripe_types')) ? 'card,ideal' : $plxPlugin->getParam('payment_stripe_types');
    # aborts
    $var['payment_stripe_maxtry'] = $plxPlugin->getParam('payment_stripe_maxtry') === '' ? 10 : intval($plxPlugin->getParam('payment_stripe_maxtry'));
    $var['payment_stripe_abortpayment'] = intval($plxPlugin->getParam('payment_stripe_abortpayment'));
    $var['payment_stripe_cancelorder'] = intval($plxPlugin->getParam('payment_stripe_cancelorder'));
    # end stripe
?><?php
  }
  public function plxMyShopConfigPayments(){
?>
   <div class="grid">
    <div class="col sml-8 lrg-5 label-centered">
     <label for="id_payment_stripe"><?php $this->lang('L_CONFIG_PAYMENT_STRIPE');?>&nbsp;:</label>
    </div>
    <div class="col sml-3 sml-push-1 lgr-push-5">
     <label class="switch switch-left-right">
      <input class="switch-input" id="id_payment_stripe" name="payment_stripe" type="checkbox"<?='<?=(empty($var["payment_stripe"]) ? "" : " checked=\"checked\"");?>'; ?> onchange="if (this.checked){document.getElementById('blockstripe').style.display='block';}else{document.getElementById('blockstripe').style.display='none';}" />
      <span class="switch-label" data-on="<?=L_YES ?>" data-off="<?=L_NO ?>"></span>
      <span class="switch-handle"></span>
     </label>
    </div>
   </div>

  <fieldset id="blockstripe" align="center" style="border:1px solid #333;display:<?='<?=($var["payment_stripe"]==1?"block":"none");?>';?>;">
   <legend><?php $this->lang('L_CONFIG_CONF_STRIPE') ?></legend>
   <div class="grid">
    <div class="col sml-12 med-5 label-centered">
     <label for="payment_stripe_amount"><?php $this->lang('L_CONFIG_AMOUNT_STRIPE') ?>&nbsp;(&gt; 0.5<?='<?=$var["devise"];?>';?>)&nbsp;:</label>
    </div>
    <div class="col sml-12 med-7">
     <input name='payment_stripe_amount' min="0.5" step="0.1" value="<?='<?=$var["payment_stripe_amount"];?>'; ?>" type="number" /><?='<?=$var["devise"];?>';?>
    </div>
   </div>
   <div class="grid">
    <div class="col sml-12 med-5 label-centered">
     <label for="payment_stripe_types"><?php $this->lang('L_CONFIG_TYPES_STRIPE') ?>&nbsp;(card,ideal,sepa_debit):</label>
    </div>
    <div class="col sml-12 med-7">
     <input name='payment_stripe_types' value="<?='<?=$var["payment_stripe_types"];?>'; ?>" type="text" />
    </div>
   </div>
   <div class="grid">
    <div class="col sml-12 med-5 label-centered">
     <label for="payment_stripe_user"><?php $this->lang('L_CONFIG_USER_STRIPE') ?>&nbsp;:</label>
    </div>
    <div class="col sml-12 med-7">
     <input name='payment_stripe_user' value="<?='<?=$var["payment_stripe_user"];?>'; ?>" type="text" />
    </div>
   </div>
   <div class="grid">
    <div class="col sml-12 med-5 label-centered">
     <label for="payment_stripe_key"><?php $this->lang('L_CONFIG_KEY_STRIPE') ?>&nbsp;:</label>
    </div>
    <div class="col sml-12 med-7">
     <input name='payment_stripe_key' value="<?='<?=$var["payment_stripe_key"];?>'; ?>" type="text" />
    </div>
   </div>
   <div class="grid">
    <div class="col sml-12 med-5 label-centered">
     <label for="payment_stripe_currencycode"><?php $this->lang('L_CONFIG_CURRENCY_STRIPE') ?>&nbsp;:&nbsp;(<?='<?=$dFlag;?>';?>)</label>
    </div>
    <div class="col sml-12 med-7">
     <input name='payment_stripe_currencycode' value="<?='<?=$var["payment_stripe_currencycode"];?>';?>" type="text" placeholder="<?='<?=$var["payment_stripe_currencycode"];?>';?>" title="<?='<?=$var["payment_stripe_currencycode"];?>';?>"/>
    </div>
   </div>
   <div class="grid"><!-- Max Try berfor auto abort -->
    <div class="col sml-12 med-5 label-centered">
     <label for="payment_stripe_maxtry"><?php $this->lang('L_CONFIG_MAXTRY_STRIPE') ?>&nbsp;:</label>
    </div>
    <div class="col sml-12 med-7">
     <input name='payment_stripe_maxtry' value="<?='<?=$var["payment_stripe_maxtry"];?>';?>" type="number" min="1" step="1" />
    </div>
   </div>
   <div class="grid"><!-- abort -->
    <div class="col sml-8 lrg-5 label-centered">
     <label for="id_payment_stripe_abortpayment"><?php $this->lang('L_CONFIG_ABORT_PAYMENT_STRIPE');?>&nbsp;:</label>
    </div>
    <div class="col sml-3 sml-push-1 lgr-push-5">
     <label class="switch switch-left-right">
      <input class="switch-input" id="id_payment_stripe_abortpayment" name="payment_stripe_abortpayment" type="checkbox"<?='<?=(!$var["payment_stripe_abortpayment"] ? "" : " checked=\"checked\"");?>'; ?> />
      <span class="switch-label" data-on="<?=L_YES ?>" data-off="<?=L_NO ?>"></span>
      <span class="switch-handle"></span>
     </label>
    </div>
   </div>
   <div class="grid"><!-- cancel -->
    <div class="col sml-8 lrg-5 label-centered">
     <label for="id_payment_stripe_cancelorder"><?php $this->lang('L_CONFIG_CANCEL_ORDER_STRIPE');?>&nbsp;:</label>
    </div>
    <div class="col sml-3 sml-push-1 lgr-push-5">
     <label class="switch switch-left-right">
      <input class="switch-input" id="id_payment_stripe_cancelorder" name="payment_stripe_cancelorder" type="checkbox"<?='<?=(!$var["payment_stripe_cancelorder"] ? "" : " checked=\"checked\"");?>'; ?> />
      <span class="switch-label" data-on="<?=L_YES ?>" data-off="<?=L_NO ?>"></span>
      <span class="switch-handle"></span>
     </label>
    </div>
   </div>

  </fieldset>

<?php
  }
  public function plxMyShopValiderCommandePost(){
echo '<?php '; ?>
   if($_POST['methodpayment'] == 'stripe'){
    $methodpayment = '<?php $this->lang('L_PAYMENT_STRIPE')?>';
   }
?><?php
  }
  public function plxMyShopValiderCommande(){
echo '<?php '; ?>
   if($this->getParam('payment_stripe')){
    $this->donneesModeles['tabChoixMethodespaiement']['stripe'] = array(
     'libelle' => '<?php $this->lang('L_PAYMENT_STRIPE')?>',
     'codeOption' => 'payment_stripe'
     );
    }
?><?php
  }
  public function plxMyShopValiderCommandeEmailSwitchPayments(){
echo '<?php '; ?>
    #Mail de récapitulatif de commande pour le client.
    switch ($_POST['methodpayment']){
     case 'stripe' :
      $status = '<?php $this->lang('L_ONGOING')?>';
      $method = '<?php $this->lang('L_PAYMENT_STRIPE')?>';
      $msgCommand = '<h3 class="msgyeah"><?php $this->lang('L_EMAIL_CONFIRM_STRIPE')?></h3>';#Shift msg with L_EMAIL_CONFIRM_'.strtoupper($_POST['methodpayment'])
     break;
     default:
    }

?><?php
  }
  public function plxMyShopValiderCommandeEmailPayments(){# include checkout.php
echo '<?php '; ?>
   if ($_POST['methodpayment'] === 'stripe'){
    $plxPlugin = $this->plxMotor->plxPlugins->aPlugins['<?=__CLASS__ ?>'];# $this is plxMyShop instance & $plxPlugin it's me *
    require PLX_PLUGINS.'<?=__CLASS__ ?>/classes/stripe_api/checkout.php';# inside *
   }
?><?php
  }
  public function plxMyShopValiderCommandeEmailPaymentsClient(){# create session data
echo '<?php '; ?>
   if ($_POST['methodpayment'] === 'stripe'){#'SCRIPT_FILENAME' => string '/var/www/PluXml/index.php'
    $_SESSION['command_filename' . $this->plugName . $this->dLang] = rtrim($_SERVER['SCRIPT_FILENAME'], '/index.php') . ltrim($nf, '.');
    $_SESSION[$this->plugName]['prods<?=__CLASS__?>'][$this->dLang] = $_SESSION[$this->plugName]['prods'][$this->dLang];
    $_SESSION[$this->plugName]['ncart<?=__CLASS__?>'][$this->dLang] = $_SESSION[$this->plugName]['ncart'][$this->dLang];
   }
?><?php

  }
  public function plxMyShopPanierSelectMethodPayment(){
echo '<?php '; ?>
   if($plxPlugin->getParam('payment_stripe')){
    $d['tabChoixMethodespaiement']['stripe']['libelle'] = '<?php $this->lang('L_PAYMENT_STRIPE'); ?>';# Add lang
    if ($totalpricettc < $plxPlugin->getParam('payment_stripe_amount'))#if amount of order is below stripe amount then remove from payment options
     unset($d['tabChoixMethodespaiement']['stripe']);
    else
     $d['tabChoixMethodespaiement'] = array_reverse($d['tabChoixMethodespaiement']);# Stripe in first
   }
?><?php
  }
}