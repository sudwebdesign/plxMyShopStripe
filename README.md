# plxMyShopStripe plugin [PluXml](https://www.pluxml.org/) & [plxMyShop](https://forum.pluxml.org/discussion/6794/) + stripe

+ [de] Hinzufügen von Zahlungen mit stripe.com im plxMyShop-Plugin
+ [en] Add payments with stripe.com in plxMyShop plugin
+ [es] Añadir pagos con stripe.com en el plugin plxMyShop
+ [fr] Ajouter des paiements avec stripe.com dans le plugin plxMyShop
+ [it] Aggiungi pagamenti con stripe.com nel plugin plxMyShop
+ [nl] Betalingen toevoegen met stripe.com in plxMyShop-plug-in
+ [oc] Ajustar vacairòls paiements femna stripe.com trabucar al plugin plxMyShop
+ [pl] Dodaj płatności z stripe.com w plxMyShop plugin
+ [pt] Adicionar pagamentos com stripe.com no plugin plxMyShop
+ [ro] Adăugați plăți cu stripe.com în plugin-ul plxMyShop
+ [ru] Добавить платежи с stripe.com в plxMyShop плагин

## Update note 1.0.1
+ [de] Es wird empfohlen, vor dem Update von v1.0.0 auf 1.0.1 plxMyShop vor plxMyShopStripe zu laden (Reihenfolge in den Parametern des PluXml-Plugins).
+ [en] Before the update from 1.0.0 to 1.0.1, it's recommended that plxMyShop should be loaded before plxMyShopStripe (order in plugins parameters of PluXml).
+ [es] Se recomienda que antes de actualizar de v1.0.0 a 1.0.1 se cargue plxMyShop antes de plxMyShopStripe (ordenar en los parámetros del plugin PluXml).
+ [fr] Il est préconisé avant de faire la mise a jour de la v1.0.0 vers la 1.0.1 que plxMyShop se charge avant plxMyShopStripe (ordre dans paramètres plugins de PluXml).
+ [it] Prima di aggiornare dalla v1.0.0.0 alla 1.0.1 si raccomanda di caricare plxMyShop prima di plxMyShopStripe (ordinare nei parametri del plugin PluXml).
+ [nl] Het wordt aanbevolen om voor het updaten van v1.0.0 naar 1.0.1 dat plxMyShop wordt geladen voor plxMyShopStripe (volgorde in PluXml plugin parameters).
+ [oc] Ostalat preconise endavanç de compelir de botada a jorn de la v1.0.0 a la 1.0.1 tre plxMyShop se carga endavanç plxMyShopStripe (ordenament trabucar parametres plugins de PluXml).
+ [pl] Przed aktualizacją z wersji v1.0.0 do 1.0.1 jest zalecane, żeby plxMyShop został załadowany przed plxMyShopStripe (kolejność w parametrach wtyczki PluXml).
+ [pt] Recomenda-se antes de actualizar de v1.0.0 para 1.0.1 que o plxMyShop seja carregado antes do plxMyShopStripe (encomendar em parâmetros de plugin PluXml).
+ [ro] Este recomandat înainte de a face actualizarea v1.0.0 la 1.0.1 că sarcinile plxMyShop înainte de plxMyShopStripe (pentru în setările plugin-uri de PluXml).
+ [ru] Перед обновлением с версии 1.0.0 до 1.0.1 рекомендуется загрузить plxMyShop перед plxMyShopStripe (заказ в параметрах плагина PluXml).
