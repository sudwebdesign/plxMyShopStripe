<?php
$LANG = array(
'CURRENCYCODE'                  => 'RUB',
'STRIPE_ALT'                    => 'Заказать с помощью кредитной карты',
'L_SHOW_PAYMENT_FORM'           => 'Показать форму оплаты',
'L_PAY'                         => 'Оплатить',
# ajax modifie commande
'L_PAID'                        => 'оплаченный',# create modifie commande
'L_ABORTED'                     => 'Заброшенный платеж',# create modifie commande
'L_CANCELED'                    => 'отменен',# create modifie commande
'L_ABORT'                       => 'Отказаться от оплаты',# Bouton
'L_CANCEL'                      => 'Отмена заказа',# Bouton

'L_STRIPE_SUCCESS'              => 'Оплата прошла успешно.',
'L_STRIPE_ABORTED'              => 'Заброшенный платеж.',
'L_STRIPE_CANCELED'             => 'Заказ отменен.',
'L_STRIPE_DASH'                 => 'Посмотреть результат на Stripe',
'L_STRIPE_GO_HOME'              => 'Вернуться на сайт',

'L_LOAD_ERROR'                  => 'Ошибка, невозможно начать Stripe.',
'L_RELOAD'                      => 'Повторить',

'L_CONFIG_PAYMENT_STRIPE'       => 'Оплата СТРИПОМ',
'L_CONFIG_CONF_STRIPE'          => 'Конфигурация полосы',
'L_CONFIG_TYPES_STRIPE'         => 'Виды способов оплаты',
'L_CONFIG_USER_STRIPE'          => 'Открытый ключ Stripe (ApiKey)',
'L_CONFIG_KEY_STRIPE'           => 'Полосатый секретный ключ (ApiKey)',
'L_CONFIG_CURRENCY_STRIPE'      => 'Код валюты',
'L_CONFIG_MAXTRY_STRIPE'        => 'Сколько попыток перед отменой платежа',
'L_CONFIG_ABORT_PAYMENT_STRIPE' => 'Разрешить отмену платежа',
'L_CONFIG_CANCEL_ORDER_STRIPE'  => 'Разрешить отмену заказа',
# plxMyShop.php
'L_PAYMENT_STRIPE'              => 'нашивка',
'L_ONGOING'                     => 'Классы',
'L_EMAIL_CONFIRM_STRIPE'        => 'Заказ подтвержден и ожидает подтверждения от вас в Stripe.',
'L_EMAIL_CUST_STRIPE'           => 'Этот заказ будет завершен после проверки оплаты Stripe.',
'L_CONFIG_AMOUNT_STRIPE'        => 'Минимальная сумма для его активации',
);