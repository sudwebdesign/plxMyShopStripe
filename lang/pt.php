<?php
$LANG = array(
'CURRENCYCODE'                  => 'EUR',
'STRIPE_ALT'                    => 'Encomendar com cartão de crédito',
'L_SHOW_PAYMENT_FORM'           => 'Exibir o formulário de pagamento',
'L_PAY'                         => 'Pagamento',
# ajax modifie commande
'L_PAID'                        => 'Pago',# create modifie commande
'L_ABORTED'                     => 'Pagamento abandonado',# create modifie commande
'L_CANCELED'                    => 'Cancelado',# create modifie commande
'L_ABORT'                       => 'Abandono do pagamento',# Bouton
'L_CANCEL'                      => 'Cancele o pedido',# Bouton

'L_STRIPE_SUCCESS'              => 'Pagamento bem sucedido.',
'L_STRIPE_ABORTED'              => 'Pagamento abandonado.',
'L_STRIPE_CANCELED'             => 'Ordem cancelada.',
'L_STRIPE_DASH'                 => 'Veja o resultado no Stripe',
'L_STRIPE_GO_HOME'              => 'Voltar ao site',

'L_LOAD_ERROR'                  => 'Erro, impossível de iniciar o Stripe.',
'L_RELOAD'                      => 'Repetir',

'L_CONFIG_PAYMENT_STRIPE'       => 'Pagamento por STRIPE',
'L_CONFIG_CONF_STRIPE'          => 'Configuração de distribuição',
'L_CONFIG_TYPES_STRIPE'         => 'Tipos de métodos de pagamento',
'L_CONFIG_USER_STRIPE'          => 'Chave pública de distribuição (ApiKey)',
'L_CONFIG_KEY_STRIPE'           => 'Chave secreta de distribuição (ApiKey)',
'L_CONFIG_CURRENCY_STRIPE'      => 'Código da moeda',
'L_CONFIG_MAXTRY_STRIPE'        => 'Quantas tentativas antes de cancelar o pagamento',
'L_CONFIG_ABORT_PAYMENT_STRIPE' => 'Permitir que o pagamento seja cancelado',
'L_CONFIG_CANCEL_ORDER_STRIPE'  => 'Permita que a ordem seja cancelada',
# plxMyShop.php
'L_PAYMENT_STRIPE'              => 'Listra',
'L_ONGOING'                     => 'Aulas',
'L_EMAIL_CONFIRM_STRIPE'        => 'O pedido está confirmado e aguarda validação sua no Stripe.',
'L_EMAIL_CUST_STRIPE'           => 'Esse pedido será finalizado após a verificação do pagamento do Stripe.',
'L_CONFIG_AMOUNT_STRIPE'        => 'Quantidade mínima para ativá-lo',
);