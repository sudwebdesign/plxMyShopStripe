<?php
$LANG = array(
'CURRENCYCODE'                  => 'EUR',
'STRIPE_ALT'                    => 'Ordenar con tarjeta de crédito',
'L_SHOW_PAYMENT_FORM'           => 'Mostrar el formulario de pago',
'L_PAY'                         => 'Paga',
# ajax modifie commande
'L_PAID'                        => 'Pagado',# create modifie commande
'L_ABORTED'                     => 'Pago abandonado',# create modifie commande
'L_CANCELED'                    => 'Cancelado',# create modifie commande
'L_ABORT'                       => 'Abandonar el pago',# Bouton
'L_CANCEL'                      => 'Cancelar el pedido',# Bouton

'L_STRIPE_SUCCESS'              => 'El pago se realiza correctamente.',
'L_STRIPE_ABORTED'              => 'Pago abandonado.',
'L_STRIPE_CANCELED'             => 'Orden cancelada.',
'L_STRIPE_DASH'                 => 'Ver el resultado en Stripe',
'L_STRIPE_GO_HOME'              => 'Volver al sitio',

'L_LOAD_ERROR'                  => 'Error, imposible iniciar Stripe.',
'L_RELOAD'                      => 'Reintentar',

'L_CONFIG_PAYMENT_STRIPE'       => 'Pago por STRIPE',
'L_CONFIG_CONF_STRIPE'          => 'Configuración de rayas',
'L_CONFIG_TYPES_STRIPE'         => 'Tipos de métodos de pago',
'L_CONFIG_USER_STRIPE'          => 'Stripe public key (ApiKey)',
'L_CONFIG_KEY_STRIPE'           => 'Stripe secret key (ApiKey)',
'L_CONFIG_CURRENCY_STRIPE'      => 'Código de moneda',
'L_CONFIG_MAXTRY_STRIPE'        => 'Cuántos intentos antes de cancelar el pago',
'L_CONFIG_ABORT_PAYMENT_STRIPE' => 'Permitir que el pago sea cancelado',
'L_CONFIG_CANCEL_ORDER_STRIPE'  => 'Permitir que el pedido sea cancelado',
# plxMyShop.php
'L_PAYMENT_STRIPE'              => 'Raya',
'L_ONGOING'                     => 'Clase',
'L_EMAIL_CONFIRM_STRIPE'        => 'El pedido está confirmado y está esperando su validación en Stripe.',
'L_EMAIL_CUST_STRIPE'           => 'Este pedido se finalizará una vez que se haya verificado el pago de Stripe.',
'L_CONFIG_AMOUNT_STRIPE'        => 'Cantidad mínima para activarlo',
);