<?php
$LANG = array(
'CURRENCYCODE'                  => 'EUR',
'STRIPE_ALT'                    => 'Zamów za pomocą karty kredytowej',
'L_SHOW_PAYMENT_FORM'           => 'Wyświetl formularz płatności',
'L_PAY'                         => 'Zapłacić',
# ajax modifie commande
'L_PAID'                        => 'Płatny',# create modifie commande
'L_ABORTED'                     => 'Zaniechiona płatność',# create modifie commande
'L_CANCELED'                    => 'Anulowany',# create modifie commande
'L_ABORT'                       => 'Zrezygnuj z płatności',# Bouton
'L_CANCEL'                      => 'Anulowanie zamówienia',# Bouton

'L_STRIPE_SUCCESS'              => 'Płatność powiodła się.',
'L_STRIPE_ABORTED'              => 'Porzucona płatność.',
'L_STRIPE_CANCELED'             => 'Zamówienie anulowane.',
'L_STRIPE_DASH'                 => 'Zobacz wynik w Stripe',
'L_STRIPE_GO_HOME'              => 'Wróć do strony',

'L_LOAD_ERROR'                  => 'Błąd, nie można uruchomić Stripe.',
'L_RELOAD'                      => 'Ponów próbę',

'L_CONFIG_PAYMENT_STRIPE'       => 'Płatność przez STRIPE',
'L_CONFIG_CONF_STRIPE'          => 'Konfiguracja pasków',
'L_CONFIG_TYPES_STRIPE'         => 'Rodzaje metod płatności',
'L_CONFIG_USER_STRIPE'          => 'Klucz publiczny Stripe (ApiKey)',
'L_CONFIG_KEY_STRIPE'           => 'Klucz tajny w paski (ApiKey)',
'L_CONFIG_CURRENCY_STRIPE'      => 'Kod waluty',
'L_CONFIG_MAXTRY_STRIPE'        => 'Ile prób przed anulowaniem płatności',
'L_CONFIG_ABORT_PAYMENT_STRIPE' => 'Zezwalaj na anulowanie płatności',
'L_CONFIG_CANCEL_ORDER_STRIPE'  => 'Zezwalaj na anulowanie zamówienia',
# plxMyShop.php
'L_PAYMENT_STRIPE'              => 'Naszywka',
'L_ONGOING'                     => 'Klasy',
'L_EMAIL_CONFIRM_STRIPE'        => 'Zamówienie zostało potwierdzone i oczekuje na potwierdzenie od Ciebie na Stripe.',
'L_EMAIL_CUST_STRIPE'           => 'To zamówienie zostanie sfinalizowane po sprawdzeniu płatności Stripe.',
'L_CONFIG_AMOUNT_STRIPE'        => 'Minimalna kwota, aby go aktywować',
);