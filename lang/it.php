<?php
$LANG = array(
'CURRENCYCODE'                  => 'EUR',
'STRIPE_ALT'                    => 'Ordina con una carta di credito',
'L_SHOW_PAYMENT_FORM'           => 'Visualizza il modulo di pagamento',
'L_PAY'                         => 'pagare',
# ajax modifie commande
'L_PAID'                        => 'Pagato',# create modifie commande
'L_ABORTED'                     => 'Pagamento abbandonato',# create modifie commande
'L_CANCELED'                    => 'Annullato',# create modifie commande
'L_ABORT'                       => 'Abbandonare il pagamento',# Bouton
'L_CANCEL'                      => 'Annullare l’ordine',# Bouton

'L_STRIPE_SUCCESS'              => 'Il pagamento ha esito positivo.',
'L_STRIPE_ABORTED'              => 'Pagamento abbandonato.',
'L_STRIPE_CANCELED'             => 'Ordine annullato.',
'L_STRIPE_DASH'                 => 'Vedere il risultato su Stripe',
'L_STRIPE_GO_HOME'              => 'Ritorna al sito',

'L_LOAD_ERROR'                  => 'Errore, impossibile avviare Stripe.',
'L_RELOAD'                      => 'Riprova',

'L_CONFIG_PAYMENT_STRIPE'       => 'Pagamento con STRIPE',
'L_CONFIG_CONF_STRIPE'          => 'Configurazione a strisce',
'L_CONFIG_TYPES_STRIPE'         => 'Tipi di metodi di pagamento',
'L_CONFIG_USER_STRIPE'          => 'Chiave pubblica a strisce (ApiKey)',
'L_CONFIG_KEY_STRIPE'           => 'Chiave segreta a strisce (ApiKey)',
'L_CONFIG_CURRENCY_STRIPE'      => 'Codice valuta',
'L_CONFIG_MAXTRY_STRIPE'        => 'Quanti tentativi prima di annullare il pagamento',
'L_CONFIG_ABORT_PAYMENT_STRIPE' => 'Consentire l’annullamento del pagamento',
'L_CONFIG_CANCEL_ORDER_STRIPE'  => 'Consentire l’annullamento dell’ordine',
# plxMyShop.php
'L_PAYMENT_STRIPE'              => 'Banda',
'L_ONGOING'                     => 'Classi',
'L_EMAIL_CONFIRM_STRIPE'        => 'L’ordine è confermato e in attesa di convalida da parte tua su Stripe.',
'L_EMAIL_CUST_STRIPE'           => 'Questo ordine verrà finalizzato una volta verificato il pagamento Stripe.',
'L_CONFIG_AMOUNT_STRIPE'        => 'Importo minimo per attivarlo',
);