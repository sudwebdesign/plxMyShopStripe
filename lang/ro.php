<?php
$LANG = array(
'CURRENCYCODE'                  => 'RON',
'STRIPE_ALT'                    => 'Comandați cu un card de credit',
'L_SHOW_PAYMENT_FORM'           => 'Afișați formularul de plată',
'L_PAY'                         => 'A plati',
# ajax modifie commande
'L_PAID'                        => 'Plătit',# create modifie commande
'L_ABORTED'                     => 'Plată abandonată',# create modifie commande
'L_CANCELED'                    => 'Anulat',# create modifie commande
'L_ABORT'                       => 'Abandonați plata',# Bouton
'L_CANCEL'                      => 'Anularea comenzii',# Bouton

'L_STRIPE_SUCCESS'              => 'Plata reușește.',
'L_STRIPE_ABORTED'              => 'Plată abandonată.',
'L_STRIPE_CANCELED'             => 'Comandă anulată.',
'L_STRIPE_DASH'                 => 'Vedeți rezultatul la Stripe',
'L_STRIPE_GO_HOME'              => 'Reveniți pe site',

'L_LOAD_ERROR'                  => 'Eroare, imposibil de pornit Stripe.',
'L_RELOAD'                      => 'Reîncercare',

'L_CONFIG_PAYMENT_STRIPE'       => 'Plata prin STRIPE',
'L_CONFIG_CONF_STRIPE'          => 'Configurația benzii',
'L_CONFIG_TYPES_STRIPE'         => 'Tipuri de metode de plată',
'L_CONFIG_USER_STRIPE'          => 'Cheie publică cu bandă (ApiKey)',
'L_CONFIG_KEY_STRIPE'           => 'Tasta secretă de bandă (ApiKey)',
'L_CONFIG_CURRENCY_STRIPE'      => 'Cod valutar',
'L_CONFIG_MAXTRY_STRIPE'        => 'Câte încercări înainte de anularea plății',
'L_CONFIG_ABORT_PAYMENT_STRIPE' => 'Se permite anularea plății',
'L_CONFIG_CANCEL_ORDER_STRIPE'  => 'Se permite anularea comenzii',
# plxMyShop.php
'L_PAYMENT_STRIPE'              => 'Dunga',
'L_ONGOING'                     => 'Clase',
'L_EMAIL_CONFIRM_STRIPE'        => 'Comanda este confirmată și așteaptă validarea dvs. de pe Stripe.',
'L_EMAIL_CUST_STRIPE'           => 'Această comandă va fi finalizată după verificarea plății Stripe.',
'L_CONFIG_AMOUNT_STRIPE'        => 'Suma minimă pentru a o activa',
);