<?php
$LANG = array(
'CURRENCYCODE'                  => 'EUR',
'STRIPE_ALT'                    => 'Commandar amb una CB',
'L_SHOW_PAYMENT_FORM'           => 'Afficher le formulaire de Paiement',
'L_PAY'                         => 'Payer',
# ajax modifie commande
'L_PAID'                        => 'Payé',# create modifie commande
'L_ABORTED'                     => 'pagament abandonne',# create modifie commande
'L_CANCELED'                    => 'Annulee',# create modifie commande
'L_ABORT'                       => 'relenquir al pagament',# Bouton
'L_CANCEL'                      => 'Annuler de comanda',# Bouton

'L_STRIPE_SUCCESS'              => 'Paiement réussit,',
'L_STRIPE_ABORTED'              => 'pagament abandonné.',
'L_STRIPE_CANCELED'             => 'comanda annulée.',
'L_STRIPE_DASH'                 => 'véser al resultat cò Stripe',
'L_STRIPE_GO_HOME'              => 'Retourner sur le site',

'L_LOAD_ERROR'                  => 'Erreur, despodedís de demarrer Stripe.',
'L_RELOAD'                      => 'Reessayer',

'L_CONFIG_PAYMENT_STRIPE'       => 'Pagament per STRIPE',
'L_CONFIG_CONF_STRIPE'          => 'Configuracion Stripe',
'L_CONFIG_TYPES_STRIPE'         => 'Types de methodes de pagament',
'L_CONFIG_USER_STRIPE'          => 'Adreça Stripe (ApiKey public)',
'L_CONFIG_KEY_STRIPE'           => 'Adreça Stripe (ApiKey secret)',
'L_CONFIG_CURRENCY_STRIPE'      => 'Còdi Devisa',
'L_CONFIG_MAXTRY_STRIPE'        => 'quant de tentatives endavanç d annuler al pagament',
'L_CONFIG_ABORT_PAYMENT_STRIPE' => 'larguejar d’annuler al pagament',
'L_CONFIG_CANCEL_ORDER_STRIPE'  => 'larguejar d’annuler de comanda',
# plxMyShop.php
'L_PAYMENT_STRIPE'              => 'Stripe',
'L_ONGOING'                     => 'cors',
'L_EMAIL_CONFIRM_STRIPE'        => 'La commande est confirmée et en attente de validation de votre part sur Stripe.',
'L_EMAIL_CUST_STRIPE'           => 'Aquesta comanda sera terminada un còp lo pagament Stripe contrarotlat.',
'L_CONFIG_AMOUNT_STRIPE'        => 'Montant minimum per l’activar',
);