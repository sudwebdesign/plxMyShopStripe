<?php
$LANG = array(
'CURRENCYCODE'                  => 'EUR',#CHF
'STRIPE_ALT'                    => 'Commander avec une CB',
'L_SHOW_PAYMENT_FORM'           => 'Afficher le formulaire de Paiement',
'L_PAY'                         => 'Payer',
# ajax modifie commande
'L_PAID'                        => 'Payée',# create modifie commande
'L_ABORTED'                     => 'Paiement abandonné',# create modifie commande
'L_CANCELED'                    => 'Commande annulée',# create modifie commande
'L_ABORT'                       => 'Abandonner le paiement',# Bouton
'L_CANCEL'                      => 'Annuler la commande',# Bouton

'L_STRIPE_SUCCESS'              => 'Paiement réussit.',
'L_STRIPE_ABORTED'              => 'Paiement abandonné.',
'L_STRIPE_CANCELED'             => 'Commande annulée.',
'L_STRIPE_DASH'                 => 'Voir le résultat chez Stripe',
'L_STRIPE_GO_HOME'              => 'Retourner sur le site',

'L_LOAD_ERROR'                  => 'Erreur, impossible de démarrer Stripe.',
'L_RELOAD'                      => 'Réessayer',

'L_CONFIG_PAYMENT_STRIPE'       => 'Paiement par STRIPE',
'L_CONFIG_CONF_STRIPE'          => 'Configuration Stripe',
'L_CONFIG_TYPES_STRIPE'         => 'Types de méthodes de paiement',
'L_CONFIG_USER_STRIPE'          => 'Clé publique Stripe (ApiKey)',
'L_CONFIG_KEY_STRIPE'           => 'Clé secrète Stripe (ApiKey)',
'L_CONFIG_CURRENCY_STRIPE'      => 'Code Devise',
'L_CONFIG_MAXTRY_STRIPE'        => 'Combien de tentatives avant d’annuler le paiement',
'L_CONFIG_ABORT_PAYMENT_STRIPE' => 'Permettre d’annuler le paiement',
'L_CONFIG_CANCEL_ORDER_STRIPE'  => 'Permettre d’annuler la commande',
# plxMyShop.php
'L_PAYMENT_STRIPE'              => 'Stripe',
'L_ONGOING'                     => 'cours',
'L_EMAIL_CONFIRM_STRIPE'        => 'La commande est confirmée et en attente de validation de votre part sur Stripe.',
'L_EMAIL_CUST_STRIPE'           => 'Cette commande sera finalisée une fois le paiement Stripe contrôlé.',
'L_CONFIG_AMOUNT_STRIPE'        => 'Montant minimum pour l’activer',
);