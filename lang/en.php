<?php
$LANG = array(
'CURRENCYCODE'                  => 'GBP',#USD,AUD
'STRIPE_ALT'                    => 'Buy Now With Credit Cards',
'L_SHOW_PAYMENT_FORM'           => 'Show Payment Form',
'L_PAY'                         => 'Pay',
# ajax modifie commande
'L_PAID'                        => 'Paid',# create modifie commande
'L_ABORTED'                     => 'Abandoned payment',# create modifie commande
'L_CANCELED'                    => 'Canceled',# create modifie commande
'L_ABORT'                       => 'Abandon payment',# Bouton
'L_CANCEL'                      => 'Cancel the order',# Bouton

'L_STRIPE_SUCCESS'              => 'Payment succeeded.',
'L_STRIPE_ABORTED'              => 'Abandoned payment.',
'L_STRIPE_CANCELED'             => 'Order cancelled.',
'L_STRIPE_DASH'                 => 'See the result at Stripe',
'L_STRIPE_GO_HOME'              => 'Return to home page',

'L_LOAD_ERROR'                  => 'Error, impossible to start Stripe.',
'L_RELOAD'                      => 'Retry',

'L_CONFIG_PAYMENT_STRIPE'       => 'Payment by STRIPE',
'L_CONFIG_CONF_STRIPE'          => 'Stripe configuration',
'L_CONFIG_TYPES_STRIPE'         => 'Types of payment methods',
'L_CONFIG_USER_STRIPE'          => 'Public Stripe Api Key',
'L_CONFIG_KEY_STRIPE'           => 'Secret Stripe Api Key',
'L_CONFIG_CURRENCY_STRIPE'      => 'Currency code',
'L_CONFIG_MAXTRY_STRIPE'        => 'How many attempts before cancelling the payment',
'L_CONFIG_ABORT_PAYMENT_STRIPE' => 'Allow payment to be cancelled',
'L_CONFIG_CANCEL_ORDER_STRIPE'  => 'Allow the order to be cancelled',
# plxMyShop.php
'L_PAYMENT_STRIPE'              => 'Stripe',
'L_ONGOING'                     => 'ongoing',
'L_EMAIL_CONFIRM_STRIPE'        => 'The order is confirmed and awaiting validation from you.on Stripe.',
'L_EMAIL_CUST_STRIPE'           => 'This order will be finalised once the Stripe payment has been verified.',
'L_CONFIG_AMOUNT_STRIPE'        => 'Minimum amount to activate it',
);