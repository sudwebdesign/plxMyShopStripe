<?php
$LANG = array(
'CURRENCYCODE'                  => 'EUR',
'STRIPE_ALT'                    => 'Bestellen Sie mit Kreditkarte',
'L_SHOW_PAYMENT_FORM'           => 'Zeigen Sie das Zahlungsformular an',
'L_PAY'                         => 'Zahlen',
# ajax modifie commande
'L_PAID'                        => 'Bezahlt',# create modifie commande
'L_ABORTED'                     => 'Zahlung aufgegeben',# create modifie commande
'L_CANCELED'                    => 'Abgesagt',# create modifie commande
'L_ABORT'                       => 'Zahlung abbrechen',# Bouton
'L_CANCEL'                      => 'Befehl abbrechen',# Bouton

'L_STRIPE_SUCCESS'              => 'Zahlung erfolgreich.',
'L_STRIPE_ABORTED'              => 'Zahlung verworfen.',
'L_STRIPE_CANCELED'             => 'Bestellung abgesagt.',
'L_STRIPE_DASH'                 => 'Das Ergebnis bei Stripe anzeigen',
'L_STRIPE_GO_HOME'              => 'Kehren Sie zur Site zurück',

'L_LOAD_ERROR'                  => 'Fehler, wir können Stripe nicht starten.',
'L_RELOAD'                      => 'Versuchen Sie es noch einmal.',

'L_CONFIG_PAYMENT_STRIPE'       => 'Zahlung per Streifen',
'L_CONFIG_CONF_STRIPE'          => 'Streifenkonfiguration',
'L_CONFIG_TYPES_STRIPE'         => 'Arten von Zahlungsmethoden',
'L_CONFIG_USER_STRIPE'          => 'Öffentlicher Schlüsselstreifen (ApiKey)',
'L_CONFIG_KEY_STRIPE'           => 'Geheimer Streifenschlüssel (ApiKey)',
'L_CONFIG_CURRENCY_STRIPE'      => 'Währungscode',
'L_CONFIG_MAXTRY_STRIPE'        => 'Wie viele Versuche, bevor Sie die Zahlung zu stornieren',
'L_CONFIG_ABORT_PAYMENT_STRIPE' => 'Ermöglichung der Annullieren der Zahlung',
'L_CONFIG_CANCEL_ORDER_STRIPE'  => 'Bestellung stornieren lassen',
# plxMyShop.php
'L_PAYMENT_STRIPE'              => 'Streifen',
'L_ONGOING'                     => 'Klassen',
'L_EMAIL_CONFIRM_STRIPE'        => 'Die Bestellung wird bestätigt und wartet auf Ihre Bestätigung auf Stripe.',
'L_EMAIL_CUST_STRIPE'           => 'Diese Bestellung wird abgeschlossen, sobald die Stripe-Zahlung überprüft wurde.',
'L_CONFIG_AMOUNT_STRIPE'        => 'Mindestbetrag, um es zu aktivieren',
);