<?php
$LANG = array(
'CURRENCYCODE'                  => 'EUR',
'STRIPE_ALT'                    => 'Bestel met een creditcard',
'L_SHOW_PAYMENT_FORM'           => 'Geef het betalingsformulier weer',
'L_PAY'                         => 'Betalen',
# ajax modifie commande
'L_PAID'                        => 'Betaald',# create modifie commande
'L_ABORTED'                     => 'Verlaten betaling',# create modifie commande
'L_CANCELED'                    => 'Geannuleerd',# create modifie commande
'L_ABORT'                       => 'Betaling stopzetten',# Bouton
'L_CANCEL'                      => 'De bestelling annuleren',# Bouton

'L_STRIPE_SUCCESS'              => 'Betaling is gelukt.',
'L_STRIPE_ABORTED'              => 'Verlaten betaling.',
'L_STRIPE_CANCELED'             => 'Bestelling geannuleerd.',
'L_STRIPE_DASH'                 => 'Bekijk het resultaat bij Stripe',
'L_STRIPE_GO_HOME'              => 'Ga terug naar de site',

'L_LOAD_ERROR'                  => 'Fout, onmogelijk om Stripe te starten.',
'L_RELOAD'                      => 'Opnieuw',

'L_CONFIG_PAYMENT_STRIPE'       => 'Betaling door STRIPE',
'L_CONFIG_CONF_STRIPE'          => 'Stripe configuratie',
'L_CONFIG_TYPES_STRIPE'         => 'Soorten betalingsmethoden',
'L_CONFIG_USER_STRIPE'          => 'Streep openbare sleutel (ApiKey)',
'L_CONFIG_KEY_STRIPE'           => 'Stripe geheime sleutel (ApiKey)',
'L_CONFIG_CURRENCY_STRIPE'      => 'Valuta code',
'L_CONFIG_MAXTRY_STRIPE'        => 'Hoeveel pogingen voordat de betaling wordt geannuleerd',
'L_CONFIG_ABORT_PAYMENT_STRIPE' => 'Betaling toestaan dat deze wordt geannuleerd',
'L_CONFIG_CANCEL_ORDER_STRIPE'  => 'De bestelling laten annuleren',
# plxMyShop.php
'L_PAYMENT_STRIPE'              => 'Streep',
'L_ONGOING'                     => 'Klassen',
'L_EMAIL_CONFIRM_STRIPE'        => 'De bestelling is bevestigd en wacht op bevestiging door jou op Stripe.',
'L_EMAIL_CUST_STRIPE'           => 'Deze bestelling wordt definitief zodra de Stripe-betaling is gecontroleerd.',
'L_CONFIG_AMOUNT_STRIPE'        => 'Minimumbedrag om het te activeren',
);